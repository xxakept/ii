import psycopg2

connection = psycopg2.connect(

	host = "localhost",
	user = "postgres",
	password = "123",
	port="5432"
	)

connection.autocommit = True
cursor = connection.cursor()
sql_create_database = 'create database ii'
cursor.execute(sql_create_database)

connection = psycopg2.connect(

	host = "localhost",
	user = "postgres",
	password = "123",
	database = "ii"
	)


class Main():

	# CREATE TABLE
	def create():

		with connection.cursor() as cursor:

			cursor.execute(
				"""CREATE TABLE noun(
					id serial PRIMARY KEY,
					value varchar(50) NOT NULL
				)"""
			)

			cursor.execute(
				"""CREATE TABLE adjective(
					id serial PRIMARY KEY,
					value varchar(50) NOT NULL
				)"""
			)

			cursor.execute(
				"""CREATE TABLE verb(
					id serial PRIMARY KEY,
					value varchar(50) NOT NULL
				)"""
			)

			cursor.execute(
				"""CREATE TABLE pronoun(
					id serial PRIMARY KEY,
					value varchar(50) NOT NULL
				)"""
			)

			cursor.execute(
				"""CREATE TABLE adverb(
					id serial PRIMARY KEY,
					value varchar(50) NOT NULL
				)"""
			)
		Main.insertt()

	# INSERT INTO TABLE
	def insertt():
		noun = [('Дела'), ('Работа'), ('Кровать'), ('Человек'), ('Парень'), ('Студент'), ('Преподаватель'), ('Слово')]
		for i in noun:
			with connection.cursor() as cursor:
				cursor.execute(
					"""INSERT INTO noun (value) VALUES (%s);""",
					(i,)
						
				)

		adjective = [('Красивый'), ('Хороший'), ('Молодой'), ('Красный')]
		for j in adjective:
			with connection.cursor() as cursor:
				cursor.execute(
					"""INSERT INTO adjective (value) VALUES (%s);""",
					(j,)
						
				)

		verb = [('Делать'), ('Работать'), ('Сломать'), ('Писать'), ('Читать'), ('Ремонтировать'), ('Программировать'), ('Учиться')]
		for k in adjective:
			with connection.cursor() as cursor:
				cursor.execute(
					"""INSERT INTO verb (value) VALUES (%s);""",
					(k,)
						
				)

		Main.selectt()

	# SELECT FROM TABLE
	def selectt():
		
		with connection.cursor() as cursor:
			cursor.execute(
						"""SELECT * FROM noun"""
						)

			a = cursor.fetchall()

		print(a)
		
if __name__ == '__main__':
	Main.create()